
const request = require('request');

const url = 'https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY'

let error_list = []
let error_count = 0;
let count= 1
const limit = 50


function makeRequest(url) {
  return new Promise((resolve, reject) => {
      request(url, function(err, resp, body) {
          err ? reject(err) : resolve(body)
      })    
  })    
}
let setteled_requests = []
for (let i = 0; i < 50; i++) {
  setteled_requests.push(makeRequest(url))
}
const Setteled_request = async () => {
  Promise.allSettled(
      setteled_requests
    )
    .then(values =>{
      for(let value of values){
        if(value.status =='rejected'){
          error_list.push(value.reason)
          ++error_count
        }
      }
    });
}


const timeout = function () {
  return new Promise((resolve, reject) => {
    setTimeout(async () => {
      if (count * limit < 1000 ){
        try{
          /*await*/ Setteled_request();
          ++ count;
        }catch(err){
        }
        timeout()
      }
      else{
        console.log(`\n\n error count: ${error_count}\n\n errorlist: \n ${JSON.stringify(error_list)}`)
        return true;
      }
    }, 1000 * 10) //10 sec
  })
}
timeout()